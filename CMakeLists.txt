cmake_minimum_required(VERSION 3.15)
project(funzioni CXX)

add_library(funzioni src/funzioni.cpp)
target_include_directories(funzioni PUBLIC include)

set_target_properties(funzioni PROPERTIES PUBLIC_HEADER "include/funzioni.h")
install(TARGETS funzioni)
